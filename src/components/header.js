import { Link } from "gatsby"
import React from "react"
import LogoImage from '../images/4921600_logo.png';
// import Hamburger from '../images/hamburger.svg';

const Header = ({ siteTitle }) => (
  <header className="flex flex-col md:flex-row justify-between items-center mx-1 md:mx-40">
    <Link to="/" className=""><img src={LogoImage} className="w-60" alt="logo" /></Link>
    <nav className="flex flex-col md:flex-row md:space-x-6">
      <a className="text-pink-500 bold" href="#article-info">Informácie</a>
      <a className="text-pink-500 bold" href="#article-story">Náš príbeh</a>
    </nav>
  </header>
)

export default Header
