import React from "react"
import CoverImage from "../images/cover_2.jpg"

const Main = props => {
  return (
    <>
    <article className="flex flex-col justify-center mx-1 md:mx-40 bg-gray-50">
      <section className="text-center my-6 animate-fade-in-down">
        <h1
          style={{
            fontFamily: "Great Vibes",
            fontWeight: "normal",
            fontStyle: "normal",
          }}
          className="text-6xl text-pink-500"
        >
          Berieme sa
        </h1>
        <h2
          style={{
            fontFamily: "Great Vibes",
            fontWeight: "normal",
            fontStyle: "normal",
          }}
          className="text-5xl text-pink-500"
        >
          Gabriela a Marcel
        </h2>
        <p
          style={{
            fontFamily: "Great Vibes",
            fontWeight: "normal",
            fontStyle: "normal",
          }}
          className="text-3xl text-pink-500"
        >
          26.6.2021
        </p>
      </section>
      <section className="flex justify-center animate-fade-in-down">
        <div className="">
          <img className="object-cover w-full xl:max-w-screen-sm" src={CoverImage} alt="Cover"/>
        </div>
      </section>
      <section className="flex flex-col md:flex-row justify-center my-6">
        <div className="flex flex-col m-6">
          <p className="font-bold uppercase color-pink-500">obrad</p>
          <p>15:00</p>
          <p>Kaplnka Najsvätejšej Trojice</p>
          <p>Dolné Orešany 302</p>
        </div>
        <div className="flex flex-col m-6">
          <p className="font-bold uppercase">hostina</p>

          <p><a target="__blank" href="https://www.karpatskaperla.sk/">Vinárstvo Karpatská Perla</a></p>
          <p>Nádražná 57, 900 81 Šenkvice</p>
        </div>
        
      </section>
    </article>
    <article id="article-info" className="flex flex-col items-center mx-1 md:mx-40 bg-gray-50 h-screen" style={{ height: 'calc(100 * var(--vh))' }}>
      <section className="my-6 max-w-screen-md">
        <h3 className="text-3xl m-3">Informácie o svadbe</h3>
        <div className="border-2 rounded-md border-pink-100 p-3 m-3 bg-white shadow-md">
          <p>Ahojte.</p>
          <p>Poslednú júnovú sobotu v tomto roku by sme sa chceli vziať so všetkým, čo k tomu patrí a v plnej paráde.</p>
          <p>Pevne dúfame, že nám to opatrenia už na druhý pokus umožnia a budeme to môcť spolu s vami osláviť.</p>
          <p>Zatiaľ by sme vás chceli touto cestou informovať o našich plánoch a o tom, že počítame s vašou účasťou.</p>
          <p>Snáď už nebudeme musieť meniť dátum ani počet hostí. Ak áno, veríme, že budete ohľaduplní, nemáme to v dnešnej dobe jednoduché s prípravami.</p>
          <p>Ak to situácia umožní, radi vás prídeme pozvať aj osobne a oficiálne.</p>
        </div>
      </section>
    </article>
    <article id="article-story" className="flex flex-col items-center mx-1 md:mx-40 bg-gray-50 h-screen">
      <section className="my-6 max-w-screen-xl">
        <h3 className="text-3xl m-3">Náš príbeh</h3>
        <div className="border-2 rounded-md border-pink-100 p-3 m-3 bg-white shadow-md">
          <p>Je dlhý, tak nevieme ako začať. Určite ho doplníme.</p>
        </div>
      </section>
    </article>
    </>
  )
}

export default Main
