import React, { useEffect } from "react"
import Main from "../components/main"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => {
  useEffect(() => {
    document.querySelector(':root').style
        .setProperty('--vh', window.innerHeight/100 + 'px');

    window.addEventListener('resize', () => { 
      document.querySelector(':root').style
        .setProperty('--vh', window.innerHeight/100 + 'px');
    })
  }, [])

  return (
    <Layout>
      <SEO title="Home" />
      <Main />
    </Layout>
  )
}

export default IndexPage
